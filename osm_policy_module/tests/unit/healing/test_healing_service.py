# -*- coding: utf-8 -*-

# Copyright 2018 Whitestack, LLC
# *************************************************************

# This file is part of OSM Monitoring module
# All Rights Reserved to Whitestack, LLC

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at

#         http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

# For those usages not covered by the Apache License, Version 2.0 please
# contact: bdiaz@whitestack.com or glavado@whitestack.com
##
import asyncio
import datetime
from unittest import TestCase, mock

from osm_policy_module.autoscaling.service import HealingService
from osm_policy_module.common.common_db_client import CommonDbClient
from osm_policy_module.common.lcm_client import LcmClient
from osm_policy_module.common.mon_client import MonClient
from osm_policy_module.core.config import Config
from osm_policy_module.core.database import HealingActionRepository


@mock.patch.object(LcmClient, "__init__", lambda *args, **kwargs: None)
@mock.patch.object(MonClient, "__init__", lambda *args, **kwargs: None)
@mock.patch.object(CommonDbClient, "__init__", lambda *args, **kwargs: None)
class TestHealscalingService(TestCase):
    def setUp(self):
        self.config = Config()
        self.loop = asyncio.new_event_loop()

    @mock.patch.object(HealingActionRepository, "get")
    @mock.patch("osm_policy_module.core.database.db")
    def test_update_alarm_status(self, database, get_alarm):
        mock_alarm = mock.Mock()
        mock_alarm.last_status = "insufficient_data"
        get_alarm.return_value = mock_alarm

        service = HealingService(self.config)
        self.loop.run_until_complete(service.update_alarm_status("test_uuid", "alarm"))
        self.assertEqual(mock_alarm.last_status, "alarm")
        mock_alarm.save.assert_called_with()
        service = HealingService(self.config)
        self.loop.run_until_complete(service.update_alarm_status("test_uuid", "ok"))
        self.assertEqual(mock_alarm.last_status, "ok")
        mock_alarm.save.assert_called_with()
        self.loop.run_until_complete(
            service.update_alarm_status("test_uuid", "insufficient_data")
        )
        self.assertEqual(mock_alarm.last_status, "insufficient_data")
        mock_alarm.save.assert_called_with()
        """
        if self.config.get("autoheal", "enabled") == "True":
            self.loop.run_until_complete(
                service.update_alarm_status("test_uuid", "alarm")
            )
            self.assertEqual(mock_alarm.last_status, "alarm")
            mock_alarm.save.assert_called_with()

        service = HealingService(self.config)
        if self.config.get("autoheal", "enabled") == "True":
            self.loop.run_until_complete(service.update_alarm_status("test_uuid", "ok"))
            self.assertEqual(mock_alarm.last_status, "ok")
            mock_alarm.save.assert_called_with()

        service = HealingService(self.config)
        if self.config.get("autoheal", "enabled") == "True":
            self.loop.run_until_complete(
                service.update_alarm_status("test_uuid", "insufficient_data")
            )
            self.assertEqual(mock_alarm.last_status, "insufficient_data")
            mock_alarm.save.assert_called_with()
        """

    @mock.patch.object(HealingActionRepository, "list")
    @mock.patch.object(HealingActionRepository, "get")
    @mock.patch(LcmClient, "heal")
    @mock.patch("osm_policy_module.core.database.db")
    def test_handle_alarm(self, database, heal, get_alarm, list_alarms):
        mock_alarm = self._build_mock_alarm("test_id", status="alarm")
        get_alarm.return_value = mock_alarm
        service = HealingService(self.config)
        self.loop.run_until_complete(service.handle_alarm("test_id", "alarm"))
        heal.assert_called_with(
            "test_nsr_id",
            "test_vnfinstance_id",
            "test_vdur_name",
            "test_vdu_id",
            "test_vnf_member_index",
            "test_heal_type",
            "test_day1",
            "test_count_index",
        )
        """
        if self.config.get("autoheal", "enabled") == "True":
            self.loop.run_until_complete(service.handle_alarm("test_id", "alarm"))
            heal.assert_called_with(
                "test_nsr_id",
                "test_vnfinstance_id",
                "test_vdur_name",
                "test_vdu_id",
                "test_vnf_member_index",
                "test_heal_type",
                "test_day1",
                "test_count_index",
            )
        """

    def _build_mock_alarm(
        self,
        last_status="alarm",
        last_heal=datetime.datetime.min,
        cooldown_time=10,
    ):
        mock_alarm = mock.Mock()
        mock_alarm.last_status = last_status
        mock_alarm.vnf_member_index = "1"
        mock_alarm.last_heal = last_heal
        mock_alarm.cooldown_time = cooldown_time
        mock_alarm.nsr_id = "test_nsr_id"
        mock_alarm.vnfinstance_id = "test_vnfinstance_id"
        mock_alarm.vdur_name = "test_vdur_name"
        mock_alarm.vdu_id = "test_vdu_id"
        mock_alarm.vnf_member_index = "test_vnf_member_index"
        mock_alarm.heal_type = "test_heal_type"
        mock_alarm.day1 = "test_day1"
        mock_alarm.count_index = "test_count_index"
        return mock_alarm
